FROM alpine:edge
LABEL maintainer = "Gabriel Xavier | https://gabrielxavier.com"

RUN set -ex; \
        apk update && apk --no-cache add docker docker-cli-buildx; \
        \
        curl -fsSL https://goss.rocks/install | sh;

CMD ["/bin/sh"]